import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
import { HttpClientModule, provideHttpClient ,HttpClient } from '@angular/common/http'; // Import HttpClientModule and provideHttpClient


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterLink, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ToastrModule as ToastModule } from 'ngx-toastr';
import { RegisterComponent } from './register/register.component';
import { RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings } from 'ng-recaptcha';
import { HomeComponent } from './home/home.component';
import { IndexComponent } from './index/index.component';
import { ContactComponent } from './contact/contact.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    HomeComponent,
    IndexComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterLink,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxCaptchaModule,
    ToastModule.forRoot(),
    RecaptchaModule,
    RecaptchaFormsModule,
    RouterModule
  ],
  providers: 
  [ {
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: '6LdNNW8pAAAAAPQaQscbZniQjAztPO4Xry_KaSDw',
    } as RecaptchaSettings,
  },

    provideClientHydration(),
    provideHttpClient(), // Fix the typo here

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
