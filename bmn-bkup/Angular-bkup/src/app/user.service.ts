import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 isUserLoggedIn : boolean;

  constructor() { 
    this.isUserLoggedIn = false;
  }
  setIsUserLoggedIn(){
    this.isUserLoggedIn = true;
  }
  getIsUserLoggedIn() {
    return this.isUserLoggedIn
  }
}
