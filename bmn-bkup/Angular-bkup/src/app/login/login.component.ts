import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;
  registerForm!: FormGroup;
  captchaResponse: string = '';

  constructor(private router: Router, private service: UserService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      logemail: ['', [Validators.required, Validators.email]],
      logpass: ['', Validators.required]
    });

    this.registerForm = this.formBuilder.group({
      logname: ['', Validators.required],
      regemail: ['', [Validators.required, Validators.email]],
      regpass: ['', Validators.required],
      location: ['', Validators.required],
      City: ['', Validators.required],
      Pincode: ['', Validators.required]
    });
  }

  loginSubmit(loginForm: any) {
    if (loginForm.logemail == 'Food' && loginForm.logpass == 'Food') {
      this.service.setIsUserLoggedIn();
      localStorage.setItem("emailId", loginForm.logemail);
      this.router.navigate(['/login']);
    } else {
      alert("invalid credentials");
    }
  }

  validateLoginForm(): boolean {
    if (this.loginForm.invalid) {
      alert('Please enter both email and password.');
      return false;
    }
    return true;
  }

  validateRegisterForm(): boolean {
    if (this.registerForm.invalid) {
      alert('Please fill in all fields.');
      return false;
    }
    return true;
  }

  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}
