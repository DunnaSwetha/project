package com.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.login;

public interface RegistrationRepository extends JpaRepository<login , Integer> {

	public login findByEmailId(String EmailId); 
	public login findByEmailIdAndPassword(String EmailId , String PAssword);

}