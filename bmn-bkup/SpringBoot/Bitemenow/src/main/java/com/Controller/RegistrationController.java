package com.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.RegistrationService;
import com.model.login;

@RestController
public class RegistrationController {
	
	@Autowired
	private RegistrationService service;
	
	@PostMapping("/registerUser")
	public login registerUser(@RequestBody login user) throws Exception {
	    String tempEmailId = user.getEmailId();
	    if (tempEmailId != null && !tempEmailId.trim().isEmpty()) {
	        login foodObj = service.fetchUserByEmaiId(tempEmailId);
	        if (foodObj != null) {
	            throw new Exception("User with " + tempEmailId + " already exists");
	        }
	    } else {
	        throw new Exception("EmailId cannot be null or empty");
	    }

	    login foodObj = service.saveFooduser(user);
	    return foodObj;
	}
	
	@PostMapping("/login")
	public login loginUser(@RequestBody login user) throws Exception{
		String tempEmailId = user.getEmailId();
		String password = user.getPassword();
		login foodObj = null;
		if(tempEmailId != null && password != null){
			foodObj = service.fetchUserByEmailIdAndPassword(tempEmailId, password);
		}
		if(foodObj== null){
			throw new Exception("enter proper emailid and password");
		}
		return foodObj;
	}

}